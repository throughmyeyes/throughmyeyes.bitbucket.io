console.log('YAY it loaded!');

$('.hero-carousel').slick({
	autoplay:true,
	autoplaySpeed:5000,
	dots:true,
	prevArrow: '<i class="fa fa-chevron-left"></i>',
	nextArrow:'<i class="fa fa-chevron-right"></i>,',
	infinite: true,
	slidesToShow:2,
	slidesToScroll:1,
});

// var lightbox = $('.hero-gallery a').simpleLightbox();

  $('#scroll').on('click', function(e) {
    e.preventDefault();
    $('html, body').animate({ scrollTop: $('.journal-section').offset().top}, 500, 'linear');
  });


 $('#scroll-home').on('click', function(e) {
    e.preventDefault();
    $('html, body').animate({ scrollTop: $('body').offset().top}, 500, 'linear');
  });


$(window).on( 'scroll', function(){
  
  if( $(window).scrollTop() > $('.journal-section').offset().top ){
     $("#scroll-home").fadeIn(1000);
  }else{
     $("#scroll-home").fadeOut(1000);
  }
  
});