console.log('YAY it loaded!');

$('.hero-carousel').slick({
	autoplay:true,
	autoplaySpeed:5000,
	dots:true,
	prevArrow: '<i class="fa fa-chevron-left"></i>',
	nextArrow:'<i class="fa fa-chevron-right"></i>,',
	infinite: true,
	slidesToShow:2,
	slidesToScroll:1,
});

$(function() {
  $('a[href*=#]').on('click', function(e) {
    e.preventDefault();
    $('html, body').animate({ scrollTop: $($(this).attr('href')).offset().top}, 500, 'linear');
  });
});

var video = document.getElementById("myVideo");
var btn = document.getElementById("myBtn");

